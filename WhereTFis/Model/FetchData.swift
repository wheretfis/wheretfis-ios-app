//
//  FetchData.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/11/21.
//

import Foundation
class FetchData {
func fetchData(completionHandler:@escaping ([Items]) -> Void){
    
    guard let url = URL.init(string: "https://wheretfis-test.thetwitchy.com/api/items/") else {
    return}
    let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
        if error != nil || data == nil{
            print("An error has occured.")
        }else{
            if let response = String.init(data: data!, encoding: .ascii){
                let jsondata = response.data(using: .utf8)!
                let itemspaces = try! JSONDecoder().decode([Items].self, from: jsondata)
                /*Debug
                
                for item in itemspaces {
                    print("ID: \(item.id)")
                }*/
                completionHandler(itemspaces)
            }
        
    }
    })
    task.resume()
}
}
