//
//  Locations.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/11/21.
//

import Foundation

struct Locations{
    let id: String?
    let description: String?
    let img: String?
}
