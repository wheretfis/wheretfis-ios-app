//
//  Items.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/11/21.
//

import Foundation
struct Items: Codable{
    let id: String?
    let description: String?
    let notes: String?
    let is_container: Bool?
    let creation_time: String?
    let picture_b64_png: String?
    let parent_container: String?
}

/*
"id": "string",
"description": "string",
"notes": "string",
"is_container": true,
"creation_time": "2021-12-12T00:43:40.546Z",
"picture_b64_png": "string",
"last_user": {
  "id": 0,
  "full_name": "string"
},
"parent_container": "string"
*/
