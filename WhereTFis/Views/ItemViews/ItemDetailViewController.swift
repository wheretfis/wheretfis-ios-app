//
//  ItemDetailViewController.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/11/21.
//

import UIKit

class ItemDetailViewController: UIViewController {
    // Passed from segue
    var passed_id: String? = ""
    var passed_description: String? = ""
    var passed_notes: String? = ""
    var passed_is_container: Bool? = false
    var passed_creation_time: String? = ""
    var passed_picture_b64_png: String? = ""
    var passed_parent_container: String? = ""
    
    // Local to screen
    @IBOutlet weak var item_id: UINavigationItem!
    @IBOutlet weak var item_description: UILabel!
    @IBOutlet weak var item_is_container: UILabel!
    @IBOutlet weak var item_creation_time: UILabel!
    @IBOutlet weak var item_parent_container: UILabel!
    @IBOutlet weak var item_notes: UILabel!
    @IBOutlet weak var item_image: UIImageView!
    
    
    func getImage(){
        let newImageData = Data(base64Encoded: passed_picture_b64_png!)

        if let newImageData = newImageData {
            item_image.image = UIImage(data: newImageData)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        item_id.title = passed_id
        item_description.text = "Description: " + passed_description!
        item_notes.text = "Notes: " + passed_notes!
        item_is_container.text = "Is container: " + (passed_is_container?.description ?? "none")!
        item_creation_time.text = "Creation time: " + passed_creation_time!
        item_parent_container.text = "Parent container: " + (passed_parent_container?.description ?? "none")!
        getImage()
    }
    
}
