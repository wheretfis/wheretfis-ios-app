//
//  ItemViewController.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/11/21.
//

import UIKit
class ItemViewController: UITableViewController {
    //passed from locaiton view. Title
    var p_location = ""
    @IBOutlet var page_nav_itme: UINavigationItem!
    
    var itemspaces = [Items](){
        didSet{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    var fetchData = FetchData()
    
    override func viewDidLoad() {
        FetchData().fetchData{ itemarray in self.itemspaces = itemarray}
        super.viewDidLoad()
        tableView.dataSource = self
        page_nav_itme.title = p_location
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemspaces.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "itemcell")
        cell.textLabel?.text = "Item: \(self.itemspaces[indexPath.row].description!)"
        return cell
    }
    // Vars to send to detailed view controller
    var i_id: String = ""
    var i_description: String = ""
    var i_notes: String = ""
    var i_is_container: Bool = false
    var i_creation_time: String = ""
    var i_picture_b64_png: String = ""
    var i_parent_container: String = ""


    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        i_id = itemspaces[indexPath.row].id!
        print("ID set on select: \(i_id)")
        i_description = itemspaces[indexPath.row].description!
        i_notes = itemspaces[indexPath.row].notes!
        i_is_container = itemspaces[indexPath.row].is_container!
        i_creation_time = itemspaces[indexPath.row].creation_time!
        i_picture_b64_png = itemspaces[indexPath.row].picture_b64_png!
        if( itemspaces[indexPath.row].parent_container == nil){
            i_parent_container = ""
        }else{
            i_parent_container = itemspaces[indexPath.row].parent_container!
        }
        self.performSegue(withIdentifier: "IVCtoIDVC", sender: nil)
        
    }
    
        override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "IVCtoIDVC"{
        let info = segue.destination as! ItemDetailViewController
        info.passed_id = i_id
        print("passed_id: \(i_id)")
        info.passed_description = i_description
        info.passed_notes = i_notes
        info.passed_is_container = i_is_container
        info.passed_creation_time = i_creation_time
        info.passed_picture_b64_png = i_picture_b64_png
        info.passed_parent_container = i_parent_container
    }
        }
    
}
