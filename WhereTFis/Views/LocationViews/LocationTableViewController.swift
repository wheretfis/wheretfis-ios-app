//
//  LocationTableViewController.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/13/21.
//

import UIKit

class LocationTableViewController: UITableViewController {
    let locations = ["Tempe Office", "Office", "Kitchen", "Desk"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier: "locationitemcell")
        cell.textLabel?.text = self.locations[indexPath.row]
        cell.textLabel?.font = UIFont.systemFont(ofSize: 30.0)
        cell.textLabel?.textAlignment = .center
        if (indexPath.row % 2 == 0){
            cell.backgroundColor = UIColor.black
        } else {
            cell.backgroundColor = UIColor.purple
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 165.00;
    }
    
    // Vars to send to detailed view controller
    var i_location: String = ""
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        i_location = locations[indexPath.row]
        self.performSegue(withIdentifier: "LocToItem", sender: nil)
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let info = segue.destination as! ItemViewController
        info.p_location = i_location
        }
    

}
