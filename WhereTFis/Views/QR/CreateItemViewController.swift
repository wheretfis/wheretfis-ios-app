//
//  CreateItemViewController.swift
//  WhereTFis
//
//  Created by Matthew Keeley on 12/13/21.
//

import UIKit

class CreateItemViewController: UIViewController {

    @IBOutlet weak var description_input: UITextField!
    
    @IBOutlet weak var parent_container_input: UITextField!
    @IBOutlet weak var id_input: UINavigationItem!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func return_to_home(_ sender: Any) {
        //performSegue(withIdentifier: "backtoitemview", sender: nil)
    }
    
}
